package com.januar.psiapp.history;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.view.ContextThemeWrapper;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import com.januar.psiapp.R;
import com.januar.psilibrary.model.PSI24Hourly;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class SingleHistoryActivityTest extends ActivityUnitTestCase<SingleHistoryActivity> {

  private SingleHistoryActivity activity;
  private PSI24Hourly psi24Hourly = new PSI24Hourly(10, 11, 12, 13, 14, 15);

  public SingleHistoryActivityTest() {
    super(SingleHistoryActivity.class);
  }

  @Before
  @UiThreadTest
  public void setup() throws Exception {
    injectInstrumentation(InstrumentationRegistry.getInstrumentation());
    super.setUp();

    ContextThemeWrapper context = new ContextThemeWrapper(getInstrumentation().getTargetContext(),
        R.style.AppTheme_NoActionBar);
    setActivityContext(context);

    activity = startActivity(new Intent(Intent.ACTION_MAIN), null, null);
  }

  @Test
  @UiThreadTest
  public void singleHistoryActivity_PresentDataToView() {
    psi24Hourly.setTimestamp("timestamp");
    activity.presentDataToView(psi24Hourly);
    assertThat(activity.timestampView.getText(), is(String.valueOf(psi24Hourly.getTimestamp())));
    assertThat(activity.nationalView.getText(), is(String.valueOf(psi24Hourly.getNational())));
    assertThat(activity.northView.getText(), is(String.valueOf(psi24Hourly.getNorth())));
    assertThat(activity.southView.getText(), is(String.valueOf(psi24Hourly.getSouth())));
    assertThat(activity.eastView.getText(), is(String.valueOf(psi24Hourly.getEast())));
    assertThat(activity.westView.getText(), is(String.valueOf(psi24Hourly.getWest())));
    assertThat(activity.centralView.getText(), is(String.valueOf(psi24Hourly.getCentral())));
  }
}