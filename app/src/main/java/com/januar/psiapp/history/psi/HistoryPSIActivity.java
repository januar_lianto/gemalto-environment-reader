package com.januar.psiapp.history.psi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.januar.psiapp.R;
import com.januar.psiapp.history.HistoryAdapter;
import com.januar.psiapp.history.HistoryContract;
import com.januar.psiapp.history.HistoryContract.Presenter;
import com.januar.psiapp.history.SingleHistoryActivity;
import com.januar.psilibrary.PSIDataLibrary;
import com.januar.psilibrary.model.Measurement;
import java.util.ArrayList;
import java.util.HashMap;

public class HistoryPSIActivity extends AppCompatActivity implements HistoryContract.View {

  @BindView(R.id.recycler_view) RecyclerView recyclerView;
  @BindView(R.id.toolbar) Toolbar toolbar;

  private Presenter mPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_history);

    ButterKnife.bind(this);

    toolbar.setTitle("PSI History");
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    Window window = getWindow();
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

    new HistoryPSIPresenter(this, new PSIDataLibrary(this));
  }

  @Override
  public void presentDataToView(HashMap<String, Measurement> timestamps) {
    HistoryAdapter mAdapter = new HistoryAdapter(new ArrayList<>(timestamps.keySet()),
        timestamp -> startSingleHistoryActivity(timestamps.get(timestamp)));
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setAdapter(mAdapter);
  }

  private void startSingleHistoryActivity(Measurement measurement) {
    Intent mIntent = new Intent(this, SingleHistoryActivity.class);
    mIntent.putExtra("measurement", measurement);
    mIntent.putExtra("data_type", "psi");
    startActivity(mIntent);
  }

  @Override
  public void onResume() {
    super.onResume();
    mPresenter.loadHistory();
  }

  @Override
  public void setPresenter(Presenter presenter) {
    this.mPresenter = presenter;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}