package com.januar.psiapp.history;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.januar.psiapp.R;
import com.januar.psilibrary.model.Measurement;

public class SingleHistoryActivity extends AppCompatActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.timestamp_view) TextView timestampView;
  @BindView(R.id.national_view) TextView nationalView;
  @BindView(R.id.north_view) TextView northView;
  @BindView(R.id.south_view) TextView southView;
  @BindView(R.id.east_view) TextView eastView;
  @BindView(R.id.west_view) TextView westView;
  @BindView(R.id.central_view) TextView centralView;
  @BindView(R.id.type_view) TextView typeView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_single_history);

    ButterKnife.bind(this);

    String dataType = getIntent().getStringExtra("data_type");

    if (dataType != null) {
      if (dataType.equals("psi")) {
        toolbar.setTitle("PSI (Hourly) History");
        typeView.setText(R.string.psi_hourly);
      } else if (dataType.equals("pm25")) {
        toolbar.setTitle("PM25 (Hourly) History");
        typeView.setText(R.string.pm25_hourly);
      }
    }

    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    Window window = getWindow();
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

    Measurement measurement = (Measurement) getIntent().getSerializableExtra("measurement");
    presentDataToView(measurement);
  }

  void presentDataToView(Measurement measurement) {
    if (measurement != null) {
      timestampView.setText(measurement.getTimestamp());
      nationalView.setText(String.valueOf(measurement.getNational()));
      northView.setText(String.valueOf(measurement.getNorth()));
      southView.setText(String.valueOf(measurement.getSouth()));
      eastView.setText(String.valueOf(measurement.getEast()));
      westView.setText(String.valueOf(measurement.getWest()));
      centralView.setText(String.valueOf(measurement.getCentral()));
    }
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}