package com.januar.psiapp.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.januar.psiapp.R;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

  private final List<String> timestamps;
  private final TimestampClickedListener mListener;

  public HistoryAdapter(List<String> timestamps, TimestampClickedListener listener) {
    this.timestamps = timestamps;
    this.mListener = listener;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.view_history, parent, false);

    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(MyViewHolder holder, int position) {
    String timestamp = timestamps.get(position);
    holder.textView.setText(timestamp);
    holder.itemView.setOnClickListener(v -> {
      if (mListener != null) {
        mListener.onClicked(timestamp);
      }
    });
  }

  @Override
  public int getItemCount() {
    return timestamps.size();
  }

  public interface TimestampClickedListener {

    void onClicked(String timestamp);
  }

  class MyViewHolder extends RecyclerView.ViewHolder {

    final TextView textView;

    MyViewHolder(View view) {
      super(view);
      textView = view.findViewById(R.id.text_view);
    }
  }
}