package com.januar.psiapp.history;

import com.januar.psiapp.BasePresenter;
import com.januar.psiapp.BaseView;
import com.januar.psilibrary.model.Measurement;
import java.util.HashMap;

public interface HistoryContract {

  interface View extends BaseView<Presenter> {

    void presentDataToView(HashMap<String, Measurement> timestamps);

  }

  interface Presenter extends BasePresenter {

    void loadHistory();
  }
}