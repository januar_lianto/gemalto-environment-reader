package com.januar.psiapp.history.pm25;

import com.januar.psiapp.history.HistoryContract;
import com.januar.psilibrary.PSIDataLibrary;

public class HistoryPM25Presenter implements HistoryContract.Presenter {

  private final HistoryContract.View mHistoryPM25View;
  private final PSIDataLibrary mDataLibrary;

  HistoryPM25Presenter(HistoryContract.View historyPM25View, PSIDataLibrary dataLibrary) {
    mHistoryPM25View = historyPM25View;
    mDataLibrary = dataLibrary;
    mHistoryPM25View.setPresenter(this);
  }

  @Override
  public void resume() {
    loadHistory();
  }

  @Override
  public void pause() {
  }

  @Override
  public void loadHistory() {
    mDataLibrary.retrievePM25History(mHistoryPM25View::presentDataToView);
  }
}