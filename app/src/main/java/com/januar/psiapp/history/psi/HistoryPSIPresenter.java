package com.januar.psiapp.history.psi;

import com.januar.psiapp.history.HistoryContract;
import com.januar.psilibrary.PSIDataLibrary;

public class HistoryPSIPresenter implements HistoryContract.Presenter {

  private final HistoryContract.View mHistoryPSIView;
  private final PSIDataLibrary mDataLibrary;

  HistoryPSIPresenter(HistoryContract.View historyPSIView, PSIDataLibrary dataLibrary) {
    mHistoryPSIView = historyPSIView;
    mDataLibrary = dataLibrary;
    mHistoryPSIView.setPresenter(this);
  }

  @Override
  public void resume() {
    loadHistory();
  }

  @Override
  public void pause() {
  }

  @Override
  public void loadHistory() {
    mDataLibrary.retrievePSIHistory(mHistoryPSIView::presentDataToView);
  }
}