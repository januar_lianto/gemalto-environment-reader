package com.januar.psiapp;

public interface BasePresenter {

  void resume();

  void pause();

}