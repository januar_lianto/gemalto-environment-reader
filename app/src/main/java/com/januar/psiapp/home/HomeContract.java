package com.januar.psiapp.home;

import com.januar.psiapp.BasePresenter;
import com.januar.psiapp.BaseView;

interface HomeContract {

  interface View extends BaseView<Presenter> {

    void showLoadingDialog(String msg);

    void hideLoadingDialog();

    void showErrorDialog(String errMsg);

    void presentDataToView(String timestamp, int psi, int pm25);

  }

  interface Presenter extends BasePresenter {

    void onRefresh();

    void onCancel();
  }
}