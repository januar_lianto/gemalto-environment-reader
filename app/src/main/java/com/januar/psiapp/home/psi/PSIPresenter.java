package com.januar.psiapp.home.psi;

import com.januar.psiapp.home.SingleDataContract;
import com.januar.psilibrary.PSIDataLibrary;
import com.januar.psilibrary.PSIDataLibraryInterface.DataRetrievalListener;
import com.januar.psilibrary.model.Measurement;

public class PSIPresenter implements SingleDataContract.Presenter {

  private final SingleDataContract.View mPSIView;
  private final PSIDataLibrary mDataLibrary;

  public PSIPresenter(SingleDataContract.View psiView, PSIDataLibrary dataLibrary) {
    mPSIView = psiView;
    mDataLibrary = dataLibrary;
    mPSIView.setPresenter(this);
  }

  @Override
  public void onRefresh() {
    mPSIView.showLoadingDialog("Query in progress...");
    mDataLibrary.retrievePSIData(new DataRetrievalListener() {
      @Override
      public void onNetworkSuccessful(Measurement result) {
        mPSIView.presentDataToView(result);
        mPSIView.hideLoadingDialog();
      }

      @Override
      public void onNetworkError(String errMsg, Measurement localMeasurement) {
        mPSIView.presentDataToView(localMeasurement);
        mPSIView.hideLoadingDialog();
      }

      @Override
      public void onError(String errMsg) {
        mPSIView.hideLoadingDialog();
        mPSIView.showErrorDialog(errMsg);
      }
    });
  }

  @Override
  public void onCancel() {
    mPSIView.hideLoadingDialog();
    mDataLibrary.cancelRetrieval();
  }

  @Override
  public void resume() {
    onRefresh();
  }

  @Override
  public void pause() {
    mPSIView.hideLoadingDialog();
    mDataLibrary.cancelRetrieval();
  }
}