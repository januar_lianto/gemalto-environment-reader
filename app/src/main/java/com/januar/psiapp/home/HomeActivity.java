package com.januar.psiapp.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.januar.psiapp.R;
import com.januar.psiapp.history.pm25.HistoryPM25Activity;
import com.januar.psiapp.history.psi.HistoryPSIActivity;
import com.januar.psiapp.home.pm25.PM25Fragment;
import com.januar.psiapp.home.pm25.PM25Presenter;
import com.januar.psiapp.home.psi.PSIFragment;
import com.januar.psiapp.home.psi.PSIPresenter;
import com.januar.psiapp.util.ActivityUtils;
import com.januar.psilibrary.PSIDataLibrary;

public class HomeActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.nav_view) NavigationView navView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    presentHomeFragment();

    navView.setNavigationItemSelectedListener(this);
  }

  private void presentHomeFragment() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
    if (fragment == null || !(fragment instanceof HomeFragment)) {
      fragment = HomeFragment.newInstance();
      ActivityUtils
          .addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.content_frame);
    }
    new HomePresenter((HomeFragment) fragment, new PSIDataLibrary(this));
    updateLayoutForFragment("Environment Reader", 0);
  }

  private void presentPSIFragment() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
    if (fragment == null || !(fragment instanceof PSIFragment)) {
      fragment = PSIFragment.newInstance();
      ActivityUtils
          .addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.content_frame);
    }
    new PSIPresenter((PSIFragment) fragment, new PSIDataLibrary(this));
    updateLayoutForFragment("PSI (Hourly)", 1);
  }

  private void presentPM25Fragment() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
    if (fragment == null || !(fragment instanceof PM25Fragment)) {
      fragment = PM25Fragment.newInstance();
      ActivityUtils
          .addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.content_frame);
    }
    new PM25Presenter((PM25Fragment) fragment, new PSIDataLibrary(this));
    updateLayoutForFragment("PM25 (Hourly)", 2);
  }

  private void startPM25HistoryActivity() {
    Intent mIntent = new Intent(this, HistoryPM25Activity.class);
    startActivity(mIntent);
  }

  private void startPSIHistoryActivity() {
    Intent mIntent = new Intent(this, HistoryPSIActivity.class);
    startActivity(mIntent);
  }

  private void updateLayoutForFragment(String title, int navItem) {
    toolbar.setTitle(title);
    if (navItem > -1) {
      navView.getMenu().getItem(navItem).setChecked(true);
    }
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_history) {
      Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
      if (fragment instanceof PM25Fragment) {
        startPM25HistoryActivity();
      } else {
        startPSIHistoryActivity();
      }
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    int id = item.getItemId();

    switch (id) {
      case R.id.nav_home:
        presentHomeFragment();
        break;
      case R.id.nav_psi:
        presentPSIFragment();
        break;
      case R.id.nav_pm25:
        presentPM25Fragment();
        break;
    }

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

}