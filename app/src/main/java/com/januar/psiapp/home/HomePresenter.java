package com.januar.psiapp.home;

import com.januar.psilibrary.PSIDataLibrary;
import com.januar.psilibrary.PSIDataLibraryInterface.NationalDataListener;

public class HomePresenter implements HomeContract.Presenter {

  private final HomeContract.View mHomeView;
  private final PSIDataLibrary mDataLibrary;

  public HomePresenter(HomeContract.View homeView, PSIDataLibrary dataLibrary) {
    mHomeView = homeView;
    mDataLibrary = dataLibrary;
    mHomeView.setPresenter(this);
  }

  @Override
  public void onRefresh() {
    mHomeView.showLoadingDialog("Query in progress...");
    mDataLibrary.retrieveNationalData(new NationalDataListener() {
      @Override
      public void onNetworkSuccessful(String timestamp, int psi, int pm25) {
        mHomeView.presentDataToView(timestamp, psi, pm25);
        mHomeView.hideLoadingDialog();
      }

      @Override
      public void onNetworkError(String errMsg, String timestamp, int localPsi, int localPm25) {
        mHomeView.presentDataToView(timestamp, localPsi, localPm25);
        mHomeView.hideLoadingDialog();
        mHomeView.showErrorDialog(errMsg);
      }

      @Override
      public void onError(String errMsg) {
        mHomeView.hideLoadingDialog();
        mHomeView.showErrorDialog(errMsg);
      }
    });
  }

  @Override
  public void onCancel() {
    mDataLibrary.cancelRetrieval();
  }

  @Override
  public void resume() {
    onRefresh();
  }

  @Override
  public void pause() {
    mHomeView.hideLoadingDialog();
    mDataLibrary.cancelRetrieval();
  }
}