package com.januar.psiapp.home;

import com.januar.psiapp.BasePresenter;
import com.januar.psiapp.BaseView;
import com.januar.psilibrary.model.Measurement;

public interface SingleDataContract {

  interface View extends BaseView<Presenter> {

    void showLoadingDialog(String msg);

    void hideLoadingDialog();

    void showErrorDialog(String errMsg);

    void presentDataToView(Measurement measurement);

  }

  interface Presenter extends BasePresenter {

    void onRefresh();

    void onCancel();
  }
}