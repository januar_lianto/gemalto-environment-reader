package com.januar.psiapp.home.pm25;

import com.januar.psiapp.home.SingleDataContract;
import com.januar.psilibrary.PSIDataLibrary;
import com.januar.psilibrary.PSIDataLibraryInterface.DataRetrievalListener;
import com.januar.psilibrary.model.Measurement;

public class PM25Presenter implements SingleDataContract.Presenter {

  private final SingleDataContract.View mPM25View;
  private final PSIDataLibrary mDataLibrary;

  public PM25Presenter(SingleDataContract.View pm25View, PSIDataLibrary dataLibrary) {
    mPM25View = pm25View;
    mDataLibrary = dataLibrary;
    mPM25View.setPresenter(this);
  }

  @Override
  public void onRefresh() {
    mPM25View.showLoadingDialog("Query in progress...");
    mDataLibrary.retrievePM25Data(new DataRetrievalListener() {
      @Override
      public void onNetworkSuccessful(Measurement result) {
        mPM25View.presentDataToView(result);
        mPM25View.hideLoadingDialog();
      }

      @Override
      public void onNetworkError(String errMsg, Measurement localMeasurement) {
        mPM25View.presentDataToView(localMeasurement);
        mPM25View.hideLoadingDialog();
      }

      @Override
      public void onError(String errMsg) {
        mPM25View.hideLoadingDialog();
        mPM25View.showErrorDialog(errMsg);
      }
    });
  }

  @Override
  public void onCancel() {
    mPM25View.hideLoadingDialog();
    mDataLibrary.cancelRetrieval();
  }

  @Override
  public void resume() {
    onRefresh();
  }

  @Override
  public void pause() {
    mPM25View.hideLoadingDialog();
    mDataLibrary.cancelRetrieval();
  }
}