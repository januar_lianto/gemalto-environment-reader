package com.januar.psiapp.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.januar.psiapp.R;
import com.januar.psiapp.home.HomeContract.Presenter;

public class HomeFragment extends Fragment implements HomeContract.View {

  @BindView(R.id.timestamp_view) TextView timestampView;
  @BindView(R.id.psi_view) TextView psiView;
  @BindView(R.id.pm25_view) TextView pm25View;
  @BindView(R.id.refresh_btn) Button refreshBtn;

  private HomeContract.Presenter mPresenter;
  private MaterialDialog loadingDialog;
  private MaterialDialog errorDialog;

  public HomeFragment() {
    // Required empty public constructor
  }

  @NonNull
  public static HomeFragment newInstance() {
    return new HomeFragment();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_home, container, false);
    ButterKnife.bind(this, root);

    refreshBtn.setOnClickListener(view -> mPresenter.onRefresh());
    return root;
  }

  @Override
  public void onResume() {
    super.onResume();
    mPresenter.resume();
  }

  @Override
  public void onPause() {
    mPresenter.pause();
    super.onPause();
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    MenuItem item = menu.findItem(R.id.action_history);
    item.setVisible(false);
  }

  @Override
  public void showLoadingDialog(String msg) {
    loadingDialog = new MaterialDialog.Builder(getContext())
        .content(msg)
        .progress(true, 100)
        .negativeText("Cancel")
        .onNegative((dialog, which) -> {
          dialog.dismiss();
          mPresenter.onCancel();
        })
        .cancelable(false)
        .canceledOnTouchOutside(false)
        .show();
  }

  @Override
  public void hideLoadingDialog() {
    if (loadingDialog != null && loadingDialog.isShowing()) {
      loadingDialog.dismiss();
    }
  }

  @Override
  public void showErrorDialog(String errMsg) {
    errorDialog = new MaterialDialog.Builder(getContext())
        .content(String.format("Error: %s\nDo you want to retry?", errMsg))
        .positiveText("Retry")
        .negativeText("Cancel")
        .onPositive((dialog, which) -> {
          errorDialog.dismiss();
          mPresenter.onRefresh();
        })
        .onNegative((dialog, which) -> errorDialog.dismiss())
        .show();
  }

  @Override
  public void presentDataToView(String timestamp, int psi, int pm25) {
    timestampView.setText(timestamp);
    psiView.setText(String.valueOf(psi));
    pm25View.setText(String.valueOf(pm25));
  }

  @Override
  public void setPresenter(Presenter presenter) {
    this.mPresenter = presenter;
  }
}