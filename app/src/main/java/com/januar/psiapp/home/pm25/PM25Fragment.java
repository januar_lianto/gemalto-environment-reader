package com.januar.psiapp.home.pm25;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.januar.psiapp.R;
import com.januar.psiapp.home.SingleDataContract;
import com.januar.psiapp.home.SingleDataContract.Presenter;
import com.januar.psilibrary.model.Measurement;


public class PM25Fragment extends Fragment implements SingleDataContract.View {

  @BindView(R.id.timestamp_view) TextView timestampView;
  @BindView(R.id.national_view) TextView nationalView;
  @BindView(R.id.north_view) TextView northView;
  @BindView(R.id.south_view) TextView southView;
  @BindView(R.id.east_view) TextView eastView;
  @BindView(R.id.west_view) TextView westView;
  @BindView(R.id.central_view) TextView centralView;
  @BindView(R.id.refresh_btn) Button refreshBtn;

  private Presenter mPresenter;
  private MaterialDialog loadingDialog;
  private MaterialDialog errorDialog;

  public PM25Fragment() {
    // Required empty public constructor
  }

  public static PM25Fragment newInstance() {
    return new PM25Fragment();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_pm25, container, false);
    ButterKnife.bind(this, root);

    refreshBtn.setOnClickListener(v -> mPresenter.onRefresh());

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();
    mPresenter.resume();
  }

  @Override
  public void onPause() {
    mPresenter.pause();
    super.onPause();
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    MenuItem item = menu.findItem(R.id.action_history);
    item.setVisible(true);
  }

  @Override
  public void showLoadingDialog(String msg) {
    loadingDialog = new MaterialDialog.Builder(getContext())
        .content(msg)
        .progress(true, 100)
        .negativeText("Cancel")
        .onNegative((dialog, which) -> {
          dialog.dismiss();
          mPresenter.onCancel();
        })
        .cancelable(false)
        .canceledOnTouchOutside(false)
        .show();
  }

  @Override
  public void hideLoadingDialog() {
    if (loadingDialog != null && loadingDialog.isShowing()) {
      loadingDialog.dismiss();
    }
  }

  @Override
  public void showErrorDialog(String errMsg) {
    errorDialog = new MaterialDialog.Builder(getContext())
        .content(String.format("Error: %s\nDo you want to retry?", errMsg))
        .positiveText("Retry")
        .negativeText("Cancel")
        .onPositive((dialog, which) -> {
          errorDialog.dismiss();
          mPresenter.onRefresh();
        })
        .onNegative((dialog, which) -> errorDialog.dismiss())
        .show();
  }

  @Override
  public void presentDataToView(Measurement measurement) {
    timestampView.setText(measurement.getTimestamp());
    nationalView.setText(String.valueOf(measurement.getNational()));
    northView.setText(String.valueOf(measurement.getNorth()));
    southView.setText(String.valueOf(measurement.getSouth()));
    eastView.setText(String.valueOf(measurement.getEast()));
    westView.setText(String.valueOf(measurement.getWest()));
    centralView.setText(String.valueOf(measurement.getCentral()));
  }

  @Override
  public void setPresenter(Presenter presenter) {
    this.mPresenter = presenter;
  }

}