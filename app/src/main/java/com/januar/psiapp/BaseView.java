package com.januar.psiapp;

public interface BaseView<T extends BasePresenter> {

  void setPresenter(T presenter);

}