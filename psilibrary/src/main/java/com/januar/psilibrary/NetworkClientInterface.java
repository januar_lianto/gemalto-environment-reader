package com.januar.psilibrary;

import io.reactivex.annotations.NonNull;

interface NetworkClientInterface {

  /**
   * Use this method to retrieve both PM25 and PSI for 24-hourly measurement.
   * @param dateTime use this to retrieve the latest PSI readings at that moment in time (YYYY-MM-DD[T]HH:mm:ss (SGT)).
   * @param callback callback to the API call.
   */
  void retrieveData(@NonNull String dateTime, @NonNull NetworkCallbackInterface callback);

  /**
   * Use this method to cancel all requests.
   */
  void cancelAllRequests();
}
