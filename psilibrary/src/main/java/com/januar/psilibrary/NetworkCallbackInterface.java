package com.januar.psilibrary;

import com.januar.psilibrary.model.PM2524Hourly;
import com.januar.psilibrary.model.PSI24Hourly;

interface NetworkCallbackInterface {

  /**
   * This method will be called when the API call is successful.
   *
   * @param dateTime this is the same dateTime that's being used in the request.
   * @param psiHourly this is the PSI 24-hourly measurement.
   * @param pm25Hourly this is the PM25 24-hourly measurement.
   */
  void onSuccess(String dateTime, PSI24Hourly psiHourly, PM2524Hourly pm25Hourly);

  /**
   * @param statusCode this is the status code returned by the server.
   * @param errorMsg this is the error message returned by the server (if any).
   */
  void onFailure(int statusCode, String errorMsg);
}