package com.januar.psilibrary;

import com.januar.psilibrary.model.Measurement;
import java.util.HashMap;

public interface PSIDataLibraryInterface {

  /**
   * Use this method to retrieve national data for both PSI and PM25
   * @param listener the listener that will return the result
   */
  void retrieveNationalData(NationalDataListener listener);

  /**
   * Use this method to retrieve single latest PSI data
   * @param listener the listener that will return the result
   */
  void retrievePSIData(DataRetrievalListener listener);

  /**
   * Use this method to retrieve single latest PM25 data
   * @param listener the listener that will return the result
   */
  void retrievePM25Data(DataRetrievalListener listener);

  /**
   * Use this method to retrieve PSI historical data
   * @param listener the listener that will return the result
   */
  void retrievePSIHistory(HistoryRetrievalListener listener);

  /**
   * Use this method to retrieve PM25 historical data
   * @param listener the listener that will return the result
   */
  void retrievePM25History(HistoryRetrievalListener listener);

  /**
   * Use this method to cancel network call
   */
  void cancelRetrieval();

  interface DataRetrievalListener {

    void onNetworkSuccessful(Measurement result);

    void onNetworkError(String errMsg, Measurement localMeasurement);

    void onError(String errMsg);
  }

  interface HistoryRetrievalListener {

    void onResult(HashMap<String, Measurement> result);
  }

  interface NationalDataListener {

    void onNetworkSuccessful(String timestamp, int psi, int pm25);

    void onNetworkError(String errMsg, String timestamp, int localPsi, int localPm25);

    void onError(String errMsg);
  }
}