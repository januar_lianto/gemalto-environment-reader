package com.januar.psilibrary;

import static com.januar.psilibrary.LocalDataClientHelper.KEY_DATETIME;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_CENTRAL;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_EAST;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_NATIONAL;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_NORTH;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_SOUTH;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PM25_WEST;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_CENTRAL;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_EAST;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_NATIONAL;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_NORTH;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_SOUTH;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_PSI_WEST;
import static com.januar.psilibrary.LocalDataClientHelper.KEY_TABLE;

import android.content.ContentValues;
import android.content.Context;
import com.januar.psilibrary.model.Measurement;
import com.januar.psilibrary.model.PM2524Hourly;
import com.januar.psilibrary.model.PSI24Hourly;
import java.util.HashMap;
import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteConstraintException;
import net.sqlcipher.database.SQLiteDatabase;

class LocalDataClient implements LocalClientInterface {

  private final SQLiteDatabase sqlDatabase;

  LocalDataClient(Context context, String password) {
    sqlDatabase = new LocalDataClientHelper(context, password).getDatabase();
  }

  @Override
  public PSI24Hourly retrieveLatestPSIData() {
    PSI24Hourly psi24Hourly = null;
    try (Cursor cursor = sqlDatabase.rawQuery(String
        .format("SELECT %s,%s,%s,%s,%s,%s,%s FROM %s ORDER BY %s DESC LIMIT 1", KEY_DATETIME,
            KEY_PSI_NATIONAL, KEY_PSI_NORTH, KEY_PSI_SOUTH, KEY_PSI_EAST, KEY_PSI_WEST,
            KEY_PSI_CENTRAL, KEY_TABLE, KEY_DATETIME), new String[]{})) {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        String dateTime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME));
        int national = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NATIONAL));
        int north = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NORTH));
        int south = cursor.getInt(cursor.getColumnIndex(KEY_PSI_SOUTH));
        int east = cursor.getInt(cursor.getColumnIndex(KEY_PSI_EAST));
        int west = cursor.getInt(cursor.getColumnIndex(KEY_PSI_WEST));
        int central = cursor.getInt(cursor.getColumnIndex(KEY_PSI_CENTRAL));
        psi24Hourly = new PSI24Hourly(national, north, south, east, west, central);
        psi24Hourly.setTimestamp(dateTime);
      }
      return psi24Hourly;
    }
  }

  @Override
  public PM2524Hourly retrieveLatestPM25Data() {
    PM2524Hourly pm2524Hourly = null;
    try (Cursor cursor = sqlDatabase.rawQuery(String
        .format("SELECT %s,%s,%s,%s,%s,%s,%s FROM %s ORDER BY %s DESC LIMIT 1", KEY_DATETIME,
            KEY_PM25_NATIONAL, KEY_PM25_NORTH, KEY_PM25_SOUTH, KEY_PM25_EAST, KEY_PM25_WEST,
            KEY_PM25_CENTRAL, KEY_TABLE, KEY_DATETIME), new String[]{})) {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        String dateTime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME));
        int national = cursor.getInt(cursor.getColumnIndex(KEY_PM25_NATIONAL));
        int north = cursor.getInt(cursor.getColumnIndex(KEY_PM25_NORTH));
        int south = cursor.getInt(cursor.getColumnIndex(KEY_PM25_SOUTH));
        int east = cursor.getInt(cursor.getColumnIndex(KEY_PM25_EAST));
        int west = cursor.getInt(cursor.getColumnIndex(KEY_PM25_WEST));
        int central = cursor.getInt(cursor.getColumnIndex(KEY_PM25_CENTRAL));
        pm2524Hourly = new PM2524Hourly(national, north, south, east, west, central);
        pm2524Hourly.setTimestamp(dateTime);
      }
      return pm2524Hourly;
    }
  }

  @Override
  public HashMap<String, Measurement> retrieveHistoricalPSIData() {
    HashMap<String, Measurement> psi24Hourlies = new HashMap<>();
    try (Cursor cursor = sqlDatabase.rawQuery(String
        .format("SELECT %s,%s,%s,%s,%s,%s,%s FROM %s", KEY_DATETIME,
            KEY_PSI_NATIONAL, KEY_PSI_NORTH, KEY_PSI_SOUTH, KEY_PSI_EAST, KEY_PSI_WEST,
            KEY_PSI_CENTRAL, KEY_TABLE, KEY_DATETIME), new String[]{})) {
      while (cursor.moveToNext()) {
        String dateTime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME));
        int national = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NATIONAL));
        int north = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NORTH));
        int south = cursor.getInt(cursor.getColumnIndex(KEY_PSI_SOUTH));
        int east = cursor.getInt(cursor.getColumnIndex(KEY_PSI_EAST));
        int west = cursor.getInt(cursor.getColumnIndex(KEY_PSI_WEST));
        int central = cursor.getInt(cursor.getColumnIndex(KEY_PSI_CENTRAL));
        PSI24Hourly psi24Hourly = new PSI24Hourly(national, north, south, east, west, central);
        psi24Hourly.setTimestamp(dateTime);
        psi24Hourlies.put(dateTime, psi24Hourly);
      }
      return psi24Hourlies;
    }
  }

  @Override
  public HashMap<String, Measurement> retrieveHistoricalPM25Data() {
    HashMap<String, Measurement> pm2524Hourlies = new HashMap<>();
    try (Cursor cursor = sqlDatabase.rawQuery(String
        .format("SELECT %s,%s,%s,%s,%s,%s,%s FROM %s", KEY_DATETIME,
            KEY_PSI_NATIONAL, KEY_PSI_NORTH, KEY_PSI_SOUTH, KEY_PSI_EAST, KEY_PSI_WEST,
            KEY_PSI_CENTRAL, KEY_TABLE, KEY_DATETIME), new String[]{})) {
      while (cursor.moveToNext()) {
        String dateTime = cursor.getString(cursor.getColumnIndex(KEY_DATETIME));
        int national = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NATIONAL));
        int north = cursor.getInt(cursor.getColumnIndex(KEY_PSI_NORTH));
        int south = cursor.getInt(cursor.getColumnIndex(KEY_PSI_SOUTH));
        int east = cursor.getInt(cursor.getColumnIndex(KEY_PSI_EAST));
        int west = cursor.getInt(cursor.getColumnIndex(KEY_PSI_WEST));
        int central = cursor.getInt(cursor.getColumnIndex(KEY_PSI_CENTRAL));
        PM2524Hourly pm2524Hourly = new PM2524Hourly(national, north, south, east, west, central);
        pm2524Hourly.setTimestamp(dateTime);
        pm2524Hourlies.put(dateTime, pm2524Hourly);
      }
      return pm2524Hourlies;
    }
  }

  @Override
  public void insertDataFromNetwork(String dateTime, PSI24Hourly psi24Hourly,
      PM2524Hourly pm2524Hourly) {
    ContentValues initialValues = new ContentValues();

    initialValues.put(KEY_DATETIME, dateTime);
    initialValues.put(KEY_PSI_NATIONAL, psi24Hourly.getNational());
    initialValues.put(KEY_PSI_NORTH, psi24Hourly.getNorth());
    initialValues.put(KEY_PSI_SOUTH, psi24Hourly.getSouth());
    initialValues.put(KEY_PSI_EAST, psi24Hourly.getEast());
    initialValues.put(KEY_PSI_WEST, psi24Hourly.getWest());
    initialValues.put(KEY_PSI_CENTRAL, psi24Hourly.getCentral());
    initialValues.put(KEY_PM25_NATIONAL, pm2524Hourly.getNational());
    initialValues.put(KEY_PM25_NORTH, pm2524Hourly.getNorth());
    initialValues.put(KEY_PM25_SOUTH, pm2524Hourly.getSouth());
    initialValues.put(KEY_PM25_EAST, pm2524Hourly.getEast());
    initialValues.put(KEY_PM25_WEST, pm2524Hourly.getWest());
    initialValues.put(KEY_PM25_CENTRAL, pm2524Hourly.getCentral());

    try {
      sqlDatabase.insertOrThrow(KEY_TABLE, null, initialValues);
    } catch (SQLiteConstraintException exception) {
      sqlDatabase.update(KEY_TABLE, initialValues, KEY_DATETIME + "=?", new String[]{dateTime});
    }
  }

  @Override
  public void closeDB() {
    sqlDatabase.close();
  }
}