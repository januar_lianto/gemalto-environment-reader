package com.januar.psilibrary;

import com.januar.psilibrary.model.Measurement;
import com.januar.psilibrary.model.PM2524Hourly;
import com.januar.psilibrary.model.PSI24Hourly;
import java.util.HashMap;

interface LocalClientInterface {

  PSI24Hourly retrieveLatestPSIData();

  PM2524Hourly retrieveLatestPM25Data();

  HashMap<String, Measurement> retrieveHistoricalPSIData();

  HashMap<String, Measurement> retrieveHistoricalPM25Data();

  void insertDataFromNetwork(String dateTime, PSI24Hourly psi24Hourly, PM2524Hourly pm2524Hourly);

  void closeDB();
}