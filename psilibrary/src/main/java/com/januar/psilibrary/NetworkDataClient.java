package com.januar.psilibrary;

import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.januar.psilibrary.model.NetworkErrResponse;
import com.januar.psilibrary.model.NetworkOKResponse;
import com.januar.psilibrary.model.PM2524Hourly;
import com.januar.psilibrary.model.PSI24Hourly;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class NetworkDataClient implements NetworkClientInterface {

  private static final int DEFAULT_ERR_ST_CODE = -1;
  private static final String DEFAULT_ERR_MSG = "Something went wrong :(";
  private final NetworkDataAPI mAPI;
  private final List<Call> calls;

  NetworkDataClient() {
    mAPI = new NetworkDataClientHelper().getDataAPI();
    calls = new ArrayList<>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void retrieveData(String dateTime, NetworkCallbackInterface callback) {
    Call<ResponseBody> call = mAPI.retrieveData(dateTime);
    call.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(@NonNull Call<ResponseBody> call,
          @NonNull Response<ResponseBody> response) {
        Gson gson = new Gson();
        if (response.code() == 200) {
          try {
            NetworkOKResponse networkOKResponse = gson
                .fromJson(response.body().string(), NetworkOKResponse.class);
            PSI24Hourly psi24Hourly = networkOKResponse.getPSI24Hourly();
            psi24Hourly.setTimestamp(networkOKResponse.getTimestamp());
            PM2524Hourly pm2525Hourly = networkOKResponse.getPM2524Hourly();
            pm2525Hourly.setTimestamp(networkOKResponse.getTimestamp());
            callback.onSuccess(networkOKResponse.getTimestamp(), psi24Hourly, pm2525Hourly);
          } catch (IOException e) {
            callback.onFailure(DEFAULT_ERR_ST_CODE, DEFAULT_ERR_MSG);
          }
        } else {
          try {
            NetworkErrResponse networkErrResponse = gson
                .fromJson(response.errorBody().string(), NetworkErrResponse.class);
            callback.onFailure(response.code(), networkErrResponse.getMessage());
          } catch (IOException e) {
            callback.onFailure(DEFAULT_ERR_ST_CODE, DEFAULT_ERR_MSG);
          }
        }
        calls.remove(call);
      }

      @Override
      public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
        callback.onFailure(DEFAULT_ERR_ST_CODE, DEFAULT_ERR_MSG);
        calls.remove(call);
      }
    });

    calls.add(call);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelAllRequests() {
    for (Call call : calls) {
      call.cancel();
    }
    calls.clear();
  }
}