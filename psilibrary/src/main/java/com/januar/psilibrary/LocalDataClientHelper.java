package com.januar.psilibrary;

import android.content.Context;
import net.sqlcipher.database.SQLiteException;
import android.support.annotation.NonNull;
import java.io.File;
import net.sqlcipher.database.SQLiteDatabase;

class LocalDataClientHelper {

  static final String KEY_TABLE = "data";
  static final String KEY_DATETIME = "date_time";
  static final String KEY_PM25_NATIONAL = "pm25_national";
  static final String KEY_PM25_NORTH = "pm25_north";
  static final String KEY_PM25_SOUTH = "pm25_south";
  static final String KEY_PM25_EAST = "pm25_east";
  static final String KEY_PM25_WEST = "pm25_west";
  static final String KEY_PM25_CENTRAL = "pm25_central";
  static final String KEY_PSI_NATIONAL = "psi_national";
  static final String KEY_PSI_NORTH = "psi_north";
  static final String KEY_PSI_SOUTH = "psi_south";
  static final String KEY_PSI_EAST = "psi_east";
  static final String KEY_PSI_WEST = "psi_west";
  static final String KEY_PSI_CENTRAL = "psi_central";

  private static final String DB_NAME = "data.db";
  private final Context context;
  private final String password;
  private SQLiteDatabase db;

  LocalDataClientHelper(Context context, String password) {
    this.context = context;
    this.password = password;
    initializeSQLCipher();
  }

  @NonNull
  public SQLiteDatabase getDatabase() {
    closeDatabase();
    File databaseFile = context.getDatabasePath(DB_NAME);
    databaseFile.mkdirs();
    try {
      db = SQLiteDatabase.openOrCreateDatabase(databaseFile, password, null);
    } catch (SQLiteException exception) {
      databaseFile.delete();
      db = SQLiteDatabase.openOrCreateDatabase(databaseFile, password, null);
    }
    return db;
  }

  private void closeDatabase() {
    if (db != null && db.isOpen()) {
      db.close();
    }
  }

  private void initializeSQLCipher() {
    SQLiteDatabase.loadLibs(context);
    SQLiteDatabase database = getDatabase();
    database.execSQL(String.format(
        "CREATE TABLE IF NOT EXISTS %s(%s TEXT PRIMARY KEY, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER,%s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER)",
        KEY_TABLE, KEY_DATETIME, KEY_PM25_NATIONAL, KEY_PM25_NORTH, KEY_PM25_SOUTH, KEY_PM25_EAST,
        KEY_PM25_WEST, KEY_PM25_CENTRAL, KEY_PSI_NATIONAL, KEY_PSI_NORTH, KEY_PSI_SOUTH,
        KEY_PSI_EAST, KEY_PSI_WEST, KEY_PSI_CENTRAL));
    closeDatabase();
  }
}