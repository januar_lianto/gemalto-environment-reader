package com.januar.psilibrary;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import com.januar.psilibrary.model.PM2524Hourly;
import com.januar.psilibrary.model.PSI24Hourly;

public class PSIDataLibrary implements PSIDataLibraryInterface {

  private final LocalDataClient mLocalClient;
  private final NetworkDataClient mNetworkClient;

  @SuppressLint("HardwareIds")
  public PSIDataLibrary(Context context) {
    mLocalClient = new LocalDataClient(context,
        Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
    mNetworkClient = new NetworkDataClient();
  }

  @NonNull
  private String getCurrDateTimeString() {
    return DateFormat.format("yyyy-MM-ddThh:mm:ss", new java.util.Date()).toString();
  }

  @Override
  public void retrieveNationalData(NationalDataListener listener) {
    mNetworkClient.retrieveData(getCurrDateTimeString(), new NetworkCallbackInterface() {
      @Override
      public void onSuccess(String dateTime, PSI24Hourly psiHourly, PM2524Hourly pm25Hourly) {
        mLocalClient.insertDataFromNetwork(dateTime, psiHourly, pm25Hourly);
        listener.onNetworkSuccessful(dateTime, psiHourly.getNational(), pm25Hourly.getNational());
      }

      @Override
      public void onFailure(int statusCode, String errorMsg) {
        PSI24Hourly psi = mLocalClient.retrieveLatestPSIData();
        PM2524Hourly pm25 = mLocalClient.retrieveLatestPM25Data();
        if (psi != null && pm25 != null) {
          listener
              .onNetworkError(errorMsg, psi.getTimestamp(), psi.getNational(), pm25.getNational());
        } else {
          listener.onError(errorMsg);
        }
      }
    });
  }

  @Override
  public void retrievePSIData(DataRetrievalListener listener) {
    mNetworkClient.retrieveData(getCurrDateTimeString(), new NetworkCallbackInterface() {
      @Override
      public void onSuccess(String dateTime, PSI24Hourly psiHourly, PM2524Hourly pm25Hourly) {
        mLocalClient.insertDataFromNetwork(dateTime, psiHourly, pm25Hourly);
        listener.onNetworkSuccessful(psiHourly);
      }

      @Override
      public void onFailure(int statusCode, String errorMsg) {
        PSI24Hourly psi = mLocalClient.retrieveLatestPSIData();
        if (psi != null) {
          listener.onNetworkError(errorMsg, psi);
        } else {
          listener.onError(errorMsg);
        }
      }
    });
  }

  @Override
  public void retrievePM25Data(DataRetrievalListener listener) {
    mNetworkClient.retrieveData(getCurrDateTimeString(), new NetworkCallbackInterface() {
      @Override
      public void onSuccess(String dateTime, PSI24Hourly psiHourly, PM2524Hourly pm25Hourly) {
        mLocalClient.insertDataFromNetwork(dateTime, psiHourly, pm25Hourly);
        listener.onNetworkSuccessful(pm25Hourly);
      }

      @Override
      public void onFailure(int statusCode, String errorMsg) {
        PM2524Hourly pm25 = mLocalClient.retrieveLatestPM25Data();
        if (pm25 != null) {
          listener.onNetworkError(errorMsg, pm25);
        } else {
          listener.onError(errorMsg);
        }
      }
    });
  }

  @Override
  public void retrievePSIHistory(HistoryRetrievalListener listener) {
    listener.onResult(mLocalClient.retrieveHistoricalPSIData());
  }

  @Override
  public void retrievePM25History(HistoryRetrievalListener listener) {
    listener.onResult(mLocalClient.retrieveHistoricalPM25Data());
  }

  @Override
  public void cancelRetrieval() {
    mNetworkClient.cancelAllRequests();
  }

}