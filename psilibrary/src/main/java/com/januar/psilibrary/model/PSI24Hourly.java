package com.januar.psilibrary.model;

import android.annotation.SuppressLint;

public class PSI24Hourly extends Measurement {

  public PSI24Hourly(int national, int north, int south, int east, int west, int central) {
    super(national, north, south, east, west, central);
  }

  @SuppressLint("DefaultLocale")
  @Override
  public String toString() {
    return String
        .format("PSI [national=%d,north=%d,south=%d,east=%d,west=%d,central=%d]", getNational(),
            getNorth(), getSouth(), getEast(), getWest(), getCentral());
  }

}