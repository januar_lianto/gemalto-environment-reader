package com.januar.psilibrary.model;

import com.google.gson.annotations.SerializedName;

public class NetworkErrResponse {

  @SerializedName("message")
  private String message;

  public String getMessage() {
    return message;
  }
}
