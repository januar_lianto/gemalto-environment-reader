package com.januar.psilibrary.model;

import android.annotation.SuppressLint;

public class PM2524Hourly extends Measurement {

  public PM2524Hourly(int national, int north, int south, int east, int west, int central) {
    super(national, north, south, east, west, central);
  }

  @SuppressLint("DefaultLocale")
  @Override
  public String toString() {
    return String
        .format("PM25 [national=%d,north=%d,south=%d,east=%d,west=%d,central=%d]", getNational(),
            getNorth(), getSouth(), getEast(), getWest(), getCentral());
  }


}