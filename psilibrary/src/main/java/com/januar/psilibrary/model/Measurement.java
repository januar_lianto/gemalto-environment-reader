package com.januar.psilibrary.model;

import android.annotation.SuppressLint;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Measurement implements Serializable {

  @SerializedName("national")
  private final int national;
  @SerializedName("north")
  private final int north;
  @SerializedName("south")
  private final int south;
  @SerializedName("east")
  private final int east;
  @SerializedName("west")
  private final int west;
  @SerializedName("central")
  private final int central;

  private String timestamp;

  Measurement(int national, int north, int south, int east, int west, int central) {
    this.national = national;
    this.north = north;
    this.south = south;
    this.east = east;
    this.west = west;
    this.central = central;
  }

  public int getNational() {
    return national;
  }

  public int getNorth() {
    return north;
  }

  public int getSouth() {
    return south;
  }

  public int getEast() {
    return east;
  }

  public int getWest() {
    return west;
  }

  public int getCentral() {
    return central;
  }

  @SuppressLint("DefaultLocale")
  @Override
  public String toString() {
    return String
        .format("Measurement [national=%d,north=%d,south=%d,east=%d,west=%d,central=%d]", national,
            north, south, east, west, central);
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }
}
