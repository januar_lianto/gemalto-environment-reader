package com.januar.psilibrary.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class NetworkOKResponse {

  @SerializedName("items")
  private List<Item> items;

  private Item getFirstItem() {
    return items.get(0);
  }

  public PSI24Hourly getPSI24Hourly() {
    return getFirstItem().getReading().getPsi24Hourly();
  }

  public PM2524Hourly getPM2524Hourly() {
    return getFirstItem().getReading().getPm2524Hourly();
  }

  public String getTimestamp() {
    return getFirstItem().getTimestamp();
  }

  class Item {

    @SerializedName("readings")
    private Reading reading;
    @SerializedName("timestamp")
    private String timestamp;

    Reading getReading() {
      return reading;
    }

    String getTimestamp() {
      return timestamp;
    }

    class Reading {

      @SerializedName("psi_twenty_four_hourly")
      private PSI24Hourly psi24Hourly;

      @SerializedName("pm25_twenty_four_hourly")
      private PM2524Hourly pm2524Hourly;

      PSI24Hourly getPsi24Hourly() {
        return psi24Hourly;
      }

      PM2524Hourly getPm2524Hourly() {
        return pm2524Hourly;
      }
    }
  }
}
