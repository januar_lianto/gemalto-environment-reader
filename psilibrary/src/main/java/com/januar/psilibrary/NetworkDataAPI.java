package com.januar.psilibrary;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface NetworkDataAPI {

  @GET("environment/psi")
  Call<ResponseBody> retrieveData(@Query("date_time") String dateTime);
}