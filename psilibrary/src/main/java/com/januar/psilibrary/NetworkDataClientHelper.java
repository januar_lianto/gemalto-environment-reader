package com.januar.psilibrary;

import android.support.annotation.NonNull;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class NetworkDataClientHelper {

  private static final String BASE_URL = "https://api.data.gov.sg/v1/";
  private static final int TIMEOUT_SEC = 10;

  @NonNull
  private OkHttpClient createOkHttpClient() {
    final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    httpClient.addInterceptor(chain -> {
      final Request original = chain.request();
      final HttpUrl originalHttpUrl = original.url();
      final HttpUrl url = originalHttpUrl.newBuilder().build();
      final Request.Builder requestBuilder = original.newBuilder().url(url);
      final Request request = requestBuilder.build();
      return chain.proceed(request);
    });
    httpClient.connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
    httpClient.writeTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
    httpClient.readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
    return httpClient.build();
  }

  @NonNull
  private Retrofit createRetrofit() {
    return new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(createOkHttpClient())
        .build();
  }

  NetworkDataAPI getDataAPI() {
    Retrofit retrofit = createRetrofit();
    return retrofit.create(NetworkDataAPI.class);
  }

}